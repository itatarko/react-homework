import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {Star, StarBorder} from "@material-ui/icons";
import {
    Button,
    Card,
    CardActions,
    CardContent,
    CardHeader,
    IconButton,
    TextField,
    Typography,
    Grid
} from "@material-ui/core";

const filmsEndpoint = "https://swapi.co/api/films/";

const getResource = async function (endpoint, index) {
    let resourceEndpoint = index ? endpoint + index : endpoint;
    let response = await fetch(resourceEndpoint);
    if (!response.ok) {
        throw Error("Http Error: " + response.status);
    }

    return await response.json();
};

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {filmsData: [], displayFilmData: []};
        this.fetchFilms = this.fetchFilms.bind(this);
        this.updateFilmData = this.updateFilmData.bind(this)
        this.fetchFilms();
    }

    createFilmData(film) {
        return {
            title: film.title, release_date: film.release_date, opening_crawl: film.opening_crawl,
            episode_id: film.episode_id, isViewed: false
        }
    }

    updateFilmData(film) {
        let filmsData = this.state.filmsData;
        let index = filmsData.map(film => film.episode_id).indexOf(film.episode_id);
        filmsData[index] = {
            title: film.title, release_date: film.release_date, opening_crawl: film.opening_crawl,
            episode_id: film.episode_id, isViewed: film.isViewed
        };
        this.setState({
            filmsData: filmsData
        })
    }

    async fetchFilms() {
        if (this.state.filmsData.length === 0) {
            const json = await getResource(filmsEndpoint);
            const films = json.results;
            let filmsData = films.map(film => this.createFilmData(film));
            this.setState({filmsData: filmsData, displayFilmData: filmsData});
        }
    }

    search(criteria) {
        let displayFilmData = this.state.filmsData.filter(film => film.title.startsWith(criteria));
        this.setState({displayFilmData: displayFilmData});
    }

    render() {
        return (
            <div className="dashboard">
                <Grid container justify="center">
                    <SearchField search={this.search.bind(this)}/>
                </Grid>
                <FilmList films={this.state.displayFilmData} updateFilmData={this.updateFilmData}/>
            </div>
        );
    }
}

class SearchField extends React.Component {
    setSearchCriteria(criteria) {
        this.props.search(criteria);
    }

    render() {
        return (
            <TextField label="Search" type="search" variant="outlined" className="searchField"
                       onChange={(searchCriteria) => {
                           this.setSearchCriteria(searchCriteria.target.value)
                       }}
            />
        );
    }
}

class FilmList extends React.Component {
    constructor(props) {
        super(props);
        this.updateFilmData = this.updateFilmData.bind(this)
    }

    updateFilmData(film) {
        this.props.updateFilmData(film);
    }

    render() {
        return (
            <Grid container direction="row" justify="center" alignItems="flex-start">
                {this.props.films.map(film => (
                    <FilmCard key={film.episode_id.toString()} film={film} updateFilmData={this.updateFilmData}/>
                ))}
            </Grid>
        )
    }
}

class FilmCard extends React.Component {
    constructor(props) {
        super(props);
        this.updateViewedStatus = this.updateViewedStatus.bind(this);
    }

    updateFilmData(film) {
        this.props.updateFilmData(film);
    }

    updateViewedStatus() {
        let film = this.props.film;
        film.isViewed = !film.isViewed;
        this.updateFilmData(film);
    }

    render() {
        return (
            <Card className="card">
                <CardHeader
                    title={this.props.film.title}
                    titleTypographyProps={{align: "center"}}
                    subheader={this.props.film.release_date}
                    subheaderTypographyProps={{align: "center"}}
                />
                <CardContent>
                    <Typography variant="body2" color="textSecondary" component="p">
                        {this.props.film.opening_crawl}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Grid container direction="row" justify="space-between" alignItems="flex-start">
                        <IconButton onClick={this.updateViewedStatus}>
                            {this.props.film.isViewed ? <Star/> : <StarBorder/>}
                        </IconButton>
                        <Button variant="contained" color="primary">
                            Characters
                        </Button>
                    </Grid>
                </CardActions>
            </Card>
        );
    }
}

ReactDOM.render(
    <Dashboard/>,
    document.getElementById('root')
);
